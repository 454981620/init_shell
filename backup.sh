#!/bin/bash
# 数据库认证
 user="root"
 password="xxxx"
 host="127.0.0.1"
 db_name="name"
# 其它
 backup_path="/opt/backup"
 DATE=`date +%F`

# 设置导出文件的缺省权限
 umask 177
# Dump数据库到SQL文件
 /usr/bin/mysqldump --user=$user --password=$password --host=$host $db_name > $backup_path/$DATE.sql

 find $backup_path/* -mtime +30 -exec rm {} \;

  #设置定时任务
 #crontab -e
 #59 23 * * * /bin/sh /opt/backup.sh