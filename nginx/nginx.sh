#!/usr/bin/env bash

yum install -y pcre-devel pcre* openssl* zlib-devel
#wget http://img2.lvya.org/nginx-1.25.0.tar.gz

wget https://nginx.org/download/nginx-1.27.2.tar.gz





tar zxvf nginx-1.27.2.tar.gz
cd nginx-1.27.2

./configure --prefix=/usr/local/nginx  --with-http_v2_module  --with-http_ssl_module  --with-http_stub_status_module --with-pcre --with-http_gzip_static_module --with-stream


make && make install
cd ..
rm -rf nginx-1.27.2.tar.gz
rm -rf nginx-1.27.2

/bin/cp -rf  nginx.conf /usr/local/nginx/conf

echo "export PATH=\$PATH:/usr/local/nginx/sbin" >> /etc/profile
source /etc/profile
nginx

#开机启动
cat > /usr/lib/systemd/system/nginxd.service <<EOF
[Unit]
Description=nginx
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/nginx/sbin/nginx
ExecReload=/usr/local/nginx/sbin/nginx -s reload
ExecStop=/usr/local/nginx/sbin/nginx -s stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF
#systemctl enable nginxd.service
/usr/local/nginx/sbin/nginx
