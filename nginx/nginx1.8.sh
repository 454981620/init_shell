#!/bin/bash

Update_nginx()
{
	wget  http://img2.lvya.org/nginx-1.25.3.tar.gz
	tar -zxf nginx-1.25.3.tar.gz
	cd nginx-1.25.3
	./configure --user=nobody --group=nobody \
	--prefix=/usr/local/nginx \
    	--with-http_stub_status_module \
    	--with-http_ssl_module


--with-http_ssl_module


  ./configure 	--prefix=/opt/nginx --with-http_ssl_module --with-http_stub_status_module --with-pcre --with-http_gzip_static_module --with-stream

./configure --prefix=/opt/nginx --with-stream
    ./configure 	--prefix=/usr/local/nginx-1.23.4 --with-http_ssl_module --with-http_stub_status_module --with-pcre --with-http_gzip_static_module --with-stream


	make
	mv /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/nginx_bak_2
	cp objs/nginx /usr/local/nginx/sbin/nginx
	echo "查看nginx版本"
	/usr/local/nginx/sbin/nginx -t
	#kill -USR2 `cat /usr/local/nginx/logs/nginx.pid`
	kill -USR2 `ps aux | grep "nginx: master process" | grep -v grep | awk '{print $2}'`
	sleep 3
	test -f /usr/local/nginx/logs/nginx.pid.oldbin && echo OK!
	read -p "上面是否输出旧版本的pid变成oldbin和OK信息,输入yes进行下一步:" canshu
	while [ ${canshu} != "yes" || ${canshu} != "YES" || ${canshu} != "y" || ${canshu} != "Y" ];
	do
		test -f /usr/local/nginx/logs/nginx.pid.oldbin && echo OK!
		read -p "上面是否输出旧版本的pid变成oldbin和OK信息,输入yes进行下一步:" canshu
	done
        kill -WINCH `cat /usr/local/nginx-1.23.4/logs/nginx.pid.oldbin`
        kill -HUP `cat /usr/local/nginx-1.23.4/logs/nginx.pid.oldbin`
        kill -QUIT `cat /usr/local/nginx-1.23.4/logs/nginx.pid.oldbin`
        echo "查看升级后的nginx版本"
        /usr/local/nginx-1.23.4/sbin/nginx -V
	rm -rf nginx-1.18.0
}

Update_nginx_zhidong()
{
        wget https://nginx.org/download/nginx-1.25.3.tar.gz
        tar -zxf nginx-1.25.3.tar.gz
        cd nginx-1.25.3
        ./configure --user=nobody --group=nobody \
        --prefix=/usr/local/nginx \
        --with-http_stub_status_module \
        --with-http_ssl_module
        make
        mv /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/nginx_bak_${CDATE}
        cp objs/nginx /usr/local/nginx/sbin/nginx
        echo "查看nginx版本"
        /usr/local/nginx/sbin/nginx -t
	make upgrade
	if [[ $? != 0 ]];then
		echo "升级失败，请手动升级"
	else
		/usr/local/nginx/sbin/nginx -V
		rm -rf nginx-1.18.0
	fi
}

cd `dirname $0`
action=${1}
CDATE=`date '+%Y-%m-%d'`
#Update_nginx
if [[ ${action} == "update_nginx" ]];then
	Update_nginx_zhidong
else
	echo "$0 update_nginx"
	exit
fi
