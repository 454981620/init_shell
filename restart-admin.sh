#!/bin/sh
echo "test shell "
source /etc/profile



ps -ef |grep app.jar  |awk {'print $2'} | sed -e "s/^/kill -9 /g" | sh -
sleep 3

nohup java -jar -Dserver.port=8082 -Dspring.profiles.active=test  notoneless-app.jar  -xms=512M -xmx=2048M >/opt/notoneless/app.log 2>&1 &

