#!/usr/bin/env bash


echo -e "请输入数据库密码: \c"
read pwd
yum install -y libaio
wget http://img2.lvya.org/mysql-5.7.17-1.el7.x86_64.rpm-bundle.tar
tar xvf mysql-5.7.17-1.el7.x86_64.rpm-bundle.tar
rpm -ivh mysql-community-common-5.7.17-1.el7.x86_64.rpm --force --nodeps
rpm -ivh mysql-community-libs-5.7.17-1.el7.x86_64.rpm --force --nodeps
rpm -ivh mysql-community-client-5.7.17-1.el7.x86_64.rpm --force --nodeps
rpm -ivh mysql-community-server-5.7.17-1.el7.x86_64.rpm --force --nodeps

#初始化密码
mysqld --initialize
chown mysql:mysql -R /var/lib/mysql
rm -rf mysql-community*
#启动
systemctl restart mysqld.service
service mysqld restart
service mysql restart

#cat /var/log/mysqld.log
grep 'temporary password' /var/log/mysqld.log
#获取随机密码
randompwd=`grep 'temporary password' /var/log/mysqld.log| awk '{gsub(/ /,"")}1'`
mysql -uroot  -p${randompwd##*:} <<EOF
set password=password('$pwd');
use mysql;
select 'host' from user where user='root';
update user set host = '%' where user ='root';
flush privileges;
EOF




#chown mysql:mysql -R /home/data
#set password=password('OxTNiiS9PjlWIDD1KEgU71ZjZQHNxh');


