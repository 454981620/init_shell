#!/bin/sh
echo "test shell "
source /etc/profile

tomcat_home=/opt/tomcat8

rm  $tomcat_home/work/* -rf
rm  $tomcat_home/webapps/ROOT -rf
#$tomcat_home/bin/startup.sh
#休眠5秒让进程自己假死
sleep 5
ps -ef |grep tomcat8  |awk {'print $2'} | sed -e "s/^/kill -9 /g" | sh -
#trap "ps -ef |grep tomcat  |awk {'print $2'} | sed -e 's/^/kill -9 /g' | sh -" SIGINT
$tomcat_home/bin/startup.sh
tail -f $tomcat_home/logs/catalina.out