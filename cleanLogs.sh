#!/bin/bash

# 日志定时清理
status(){
  echo > /opt/tomcat8_eos1/logs/catalina.out;
  echo > /opt/tomcat8_eos2/logs/catalina.out;
  echo > /opt/tomcat8_saas/logs/catalina.out;
  echo > /usr/local/nginx/logs/access.log;
  find /opt/tomcat*/logs/* -name *.swp* -exec mv {} /opt/work_trash  \;
  echo "删除eos1,eos2,saas catalina.out 文件成功"
  return
}
status
# case "$1" in
#     status)
#         status;;*)
#             echo "清除失败"
# esac
# crontab -e
# 0 1 * * * /opt/work_sh/cleanLogs.sh