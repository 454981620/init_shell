#!/usr/bin/env bash

#install_tomcat
#wget http://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.35/bin/apache-tomcat-8.5.35.tar.gz
wget --no-check-certificate https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.97/bin/apache-tomcat-8.5.97.tar.gz
tar zxvf apache-tomcat-8.5.97.tar.gz
rm -rf apache-tomcat-8.5.97.tar.gz

mv apache-tomcat-8.5.97 /opt/tomcat8
rm -rf /opt/tomcat8/webapps/
/bin/cp -rf  server.xml /opt/tomcat8/conf

#开机启动
cat > /etc/systemd/system/tomcat.service <<EOF

[Unit]

Description=Apache Tomcat Web Application Container

After=syslog.target network.target



[Service]

Type=forking



Environment=JAVA_HOME=/usr/local/jdk1.8.0_191

Environment=CATALINA_PID=/opt/tomcat8/temp/tomcat.pid

Environment=CATALINA_HOME=/opt/tomcat8

Environment=CATALINA_BASE=/opt/tomcat8



ExecStart=/opt/tomcat8/bin/catalina.sh start

ExecStop=/bin/kill -15 $CATALINA_PID



[Install]

WantedBy=multi-user.target
EOF

#systemctl enable tomcat.service

/opt/tomcat8/bin/startup.sh
