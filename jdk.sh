#!/usr/bin/env bash

#install_jdk
wget http://img2.lvya.org/jdk-8u191-linux-x64.tar.gz
tar zxvf jdk-8u191-linux-x64.tar.gz
rm -rf jdk-8u191-linux-x64.tar.gz
mv jdk1.8.0_191 /usr/local/
ln -s /usr/local/jdk1.8.0_191/bin/java /usr/bin/java
echo -e "JAVA_HOME=/usr/local/jdk1.8.0_191;\nexport PATH=\$PATH:\$JAVA_HOME/bin" >> /etc/profile
source /etc/profile

#安全设置导致tomcat启动慢
sed -i 's/dev\/random/dev\/.\/random/g' /usr/local/jdk1.8.0_191/jre/lib/security/java.security